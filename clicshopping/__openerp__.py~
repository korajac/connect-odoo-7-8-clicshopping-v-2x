# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    This module copyright (C) 2014 e-imaginis
#    (http://www.clicshopping.org).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'ClicShopping',
    'version': '0.1',
    'maintainer': 'e-Imaginis',
    'license': 'GPL-2',
    'summary': 'Connect ClicShopping and Odoo',
      
    'description': """

This module connects Odoo and ClicShopping.

ClicShopping(TM) (http://www.clicshopping.org/) is a popular social e-commerce B2B/B2C plateform Open Source written in PHP/MySQL and published under the Open Software licence v2.0.
He has been developped by e-Imaginis (http://www.e-imaginis.com)

ClicShopping integrate natively via webservice a connexion in XML-RPC with Odoo to export the features in realtime.

- Customers (create / update)
- Customers Groups (create / update)
- Products (create / update)
- Products groups (create / update)
- Product categories (create / update)
- Suppliers (create / update)
- Orders (create /update)
- Invoice (create / update)
- Wharehouse (create)
- Stock (create / update)
- Brands (create / update)
- Claim (soon)

This module allows you to create a synchronisation Odoo to ClicShopping and export inside ClicShopping the following features in realtime.
It also take the difference between B2B and B2C (not available at this time)

- Customers  (soon)
- Customers groups
- products  (soon)
- Suppliers  (soon)
- Orders (soon)
- invoice (soon)
- Claim (soon)
- Stock  (soon)
- Categories (soon)
- Product categories (soon)
- Brands (soon)

If you have question, please push on Odoo community
Lot of videos are available on youtube, search ClicShopping

Thank you for Odoo Community for his help.

Loïc Richard 
(loic.richard@e-imaginis.com)

If you  want donate something for this work, you can via Paypal : http://goo.gl/i1Agbz

""",

    'author': 'e-Imaginis',
    'images': ['images/icon.png'],
    'website': "https://www.clicshopping.org",
    'category': "Features",

# any module necessary for this one to work correctly
    'depends': [
                'account_accountant',
                'account',
                'account_voucher',
                'sale',
                'product',
                'stock',
#                'document',
                'crm_claim',
                'claim_from_delivery',
        ],


    'external_dependencies': {
        'python': [],
    },
    
# always loaded
    'data': [
            'security/security.xml',
             'security/ir.model.access.csv',
             'views/product_clicshopping_view.xml',
             'views/res_partner.xml',
             'views/clicshopping_manufacturer_view.xml',
             'views/product_category_view.xml',
             'views/clicshopping_invoice_view.xml',
             'views/clicshopping_sale_order_view.xml',
             'views/clicshopping_config.xml',
             'views/clicshopping_customers_group.xml',
             'views/clicshopping_products_group.xml',
        ],

# only loaded in demonstration mode
    'demo': [
        ],
    
# used for Javascript Web CLient Testing with QUnit / PhantomJS
# https://www.odoo.com/documentation/8.0/reference/javascript.html#testing-in-odoo-web-client
    'js': [],
    'css': [],
    'qweb': [],

    'installable': True,
# Install this module automatically if all dependency have been previously and independently installed.
# Used for synergetic or glue modules.
    'auto_install': False,
    'application': True,
}
